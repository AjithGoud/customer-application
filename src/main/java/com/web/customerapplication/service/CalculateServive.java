package com.web.customerapplication.service;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.web.customerapplication.model.Item;
import com.web.customerapplication.repository.ItemRepository;

@Service
public class CalculateServive {

	@Autowired
	private ItemRepository itemRepo;

	public Double calculate(int id, Double intrest) {
		Item item = itemRepo.findById(id).get();
		Period period = dateCalculation(item);
		int amount = item.getAmount().intValue();

		double total = 0;
		int months = Math.abs(period.getMonths());
		int days = period.getDays();

		if (intrest == null) {
			if (amount < 10000) {
				total = amountCalculation(amount, months, 3, days);
			}
			if (amount > 10000 && amount < 30000) {
				total = amountCalculation(amount, months, 2.5, days);
			}
			if (amount > 30000) {
				total = amountCalculation(amount, months, 2, days);
			}
		} else {

			total = amountCalculation(amount, months, intrest, days);

		}
		return total;

	}

	private Period dateCalculation(Item item) {
		LocalDate date = item.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

		Period period = Period.between(LocalDate.now(), date);
		return period;

	}

	private Double amountCalculation(int amount, int months, double intrest, int days) {
		Double total = 0.0;
		if (days>0 && days <= 15 ) {
			total = (amount * intrest * (months + 0.5)) / 100;
		} else if(days>15){
			total = (amount * intrest * (months + 1)) / 100;
		}else {
			total=(amount * intrest * months) / 100;
		}
		return total;

	}
}
