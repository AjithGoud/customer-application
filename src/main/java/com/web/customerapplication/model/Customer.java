package com.web.customerapplication.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "customer")
public class Customer implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonProperty("customer_id")
	@Column(name = "customer_id")
	private int customerId;
	@JsonProperty("name")
	@Column(name = "name")
	private String name;
	@JsonProperty("son_or_daughter_of")
	@Column(name = "son_or_daughter_of")
	private String sonOf;
	@JsonProperty("address")
	@Column(name = "address")
	private String address;
	@JsonProperty("phone_number")
	@Column(name = "phone_number")
	private String phneNo;
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name = "customer_id", referencedColumnName = "customer_id")
	private List<Item> item;

	public List<Item> getItem() {
		return item;
	}

	public void setItem(List<Item> item) {
		this.item = item;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSonOf() {
		return sonOf;
	}

	public void setSonOf(String sonOf) {
		this.sonOf = sonOf;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhneNo() {
		return phneNo;
	}

	public void setPhneNo(String phneNo) {
		this.phneNo = phneNo;
	}

	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", name=" + name + ", sonOf=" + sonOf + ", address=" + address
				+ ", phneNo=" + phneNo + ", item=" + item + "]";
	}

}
