package com.web.customerapplication.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "item")
public class Item implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "item_id")
	private int id;
	@JsonProperty("serialNumber")
	@Column(name = "serialNumber")
	private int serialNumber;
	@JsonProperty("item")
	@Column(name = "item")
	private String item;
	@JsonProperty("weight")
	@Column(name = "weight")
	private Double weight;
	@JsonProperty("purity")
	@Column(name = "purity")
	private Double purity;
	@JsonProperty("amount")
	@Column(name = "amount")
	private Double amount;
	@JsonProperty("intrest")
	@Column(name = "intrest")
	private Double intrest;
	@JsonProperty("date")
	@Column(name = "date")
	private Date date;

	@Column(name = "customer_id")
	private int customerId;

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(int serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getPurity() {
		return purity;
	}

	public void setPurity(Double purity) {
		this.purity = purity;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getIntrest() {
		return intrest;
	}

	public void setIntrest(Double intrest) {
		this.intrest = intrest;
	}

	@Override
	public String toString() {
		return "Item [id=" + id + ", serialNumber=" + serialNumber + ", item=" + item + ", weight=" + weight
				+ ", purity=" + purity + ", amount=" + amount + ", intrest=" + intrest + ", date=" + date
				+ ", customerId=" + customerId + "]";
	}

}
