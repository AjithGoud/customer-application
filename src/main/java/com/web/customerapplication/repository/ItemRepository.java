package com.web.customerapplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.web.customerapplication.model.Item;

@Repository
public interface ItemRepository extends JpaRepository<Item, Integer> {

}
