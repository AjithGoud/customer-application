package com.web.customerapplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.web.customerapplication.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

	 Customer findByName(String name);

	 Customer findByPhneNo(String phneno);
}
