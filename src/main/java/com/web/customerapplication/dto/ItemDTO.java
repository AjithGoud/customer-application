package com.web.customerapplication.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ItemDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("serialNumber")
	private String serialNumber;
	@JsonProperty("item")
	private String item;
	@JsonProperty("weight")
	private Double weight;
	@JsonProperty("purity")
	private Double purity;
	@JsonProperty("amount")
	private Double amount;
	@JsonProperty("intrest")
	private Double intrest;

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getPurity() {
		return purity;
	}

	public void setPurity(Double purity) {
		this.purity = purity;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getIntrest() {
		return intrest;
	}

	public void setIntrest(Double intrest) {
		this.intrest = intrest;
	}

}
