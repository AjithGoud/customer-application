package com.web.customerapplication.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.web.customerapplication.dto.ItemDTO;
import com.web.customerapplication.model.Customer;
import com.web.customerapplication.model.Item;
import com.web.customerapplication.repository.CustomerRepository;

@RestController
@RequestMapping(value = "/api/v1")
public class ItemsController {

	@Autowired
	private CustomerRepository customerRepo;

	@PostMapping(value = "/item")
	public ResponseEntity<?> setItem(@RequestBody ItemDTO itemDto, @RequestParam int id)
			throws IllegalAccessException, InvocationTargetException {
		Customer customer = customerRepo.findById(id).get();
		Item item = new Item();
		
		item.setDate(new Date());
		BeanUtils.copyProperties(item, itemDto);
		customer.getItem().add(item);
		Customer response =customerRepo.save(customer);
		return ResponseEntity.ok().body(response);
	}
}
