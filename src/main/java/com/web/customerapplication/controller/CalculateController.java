package com.web.customerapplication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.web.customerapplication.service.CalculateServive;

@RestController
@RequestMapping(value = "/api/v1")
public class CalculateController {

	@Autowired
	private CalculateServive service;
	
  @GetMapping(value = "/calculate")
	public ResponseEntity<?> calculate(@RequestParam int id,@RequestParam Double intrest) {

		Double dat=service.calculate(id,intrest);
		return ResponseEntity.ok().body(dat);

	}
}
