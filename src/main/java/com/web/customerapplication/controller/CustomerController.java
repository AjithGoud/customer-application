package com.web.customerapplication.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.web.customerapplication.model.Customer;
import com.web.customerapplication.repository.CustomerRepository;

@RestController
@RequestMapping(value = "api/v1")
public class CustomerController {
	@Autowired
	private CustomerRepository customerRepo;

	@GetMapping(value = "/customer/setup")
	public String setUp() {
		return "application setup complete";
	}

	@GetMapping(value = "/customer")
	public ResponseEntity<?> getCustomer(@RequestParam int id) {
		Optional<Customer> customer = customerRepo.findById(id);
		if (customer.isPresent()) {
			return ResponseEntity.ok().body("no customer found");
		}
		return ResponseEntity.ok().body(customer);

	}

	@PostMapping(value = "/customer")
	public ResponseEntity<?> createCustomer(@RequestBody Customer customer) {
		Customer cust = customerRepo.findByName(customer.getName());
		Customer cust1 = customerRepo.findByPhneNo(customer.getPhneNo());
		if (cust != null) {
			return ResponseEntity.ok().body("customer exist with name : " + customer.getName());
		} else if (cust1 != null) {
			return ResponseEntity.ok().body("customer exist with phone number : " + customer.getPhneNo());
		} else {
			Customer response = customerRepo.save(customer);
			return ResponseEntity.ok().body(response);
		}
	}
	
}
